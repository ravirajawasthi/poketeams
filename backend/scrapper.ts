import mongoose from "mongoose";
import Axios from "axios";
import Move from "./models/Move";
import { MONGO_PASSWORD } from "./.env";
import Pokemon from "./models/Pokemon";
mongoose.connect(
  `mongodb+srv://bablu:${MONGO_PASSWORD}@pokemonteam.wcyto.mongodb.net/PokeTeams?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

const ToplevelAwait = async () => {
  const {
    data: { results: AllPokemons },
  } = await Axios.get("https://pokeapi.co/api/v2/pokemon/?limit=10000");

  AllPokemons.map(async (pokemon: { url: string }) => {
    await Pokemon.deleteMany({});
    const foundMove = await Move.find({});
    const { data: PokemonInfo } = await Axios.get(pokemon.url);
    let typesArray: any[] = [];
    let movesArray: any[] = [];
    let images: string[] = [];
    const sprites = PokemonInfo.sprites;
    images.push(sprites.other["official-artwork"].front_default);
    images.push(sprites.other["dream_world"].front_default);
    images.push(sprites.front_default);
    images = images.filter((url) => url !== null);
    await PokemonInfo.types.map((type: { type: { name: any } }) => {
      typesArray.push(type.type.name);
    });
    await PokemonInfo.moves.map(async (move: { move: { name: any } }) => {
      const moveName = move.move.name;
      movesArray.push(foundMove.find((move) => move.name === moveName));
    });
    const newPokemon = new Pokemon({
      name: PokemonInfo.name,
      nationalDex: PokemonInfo.order,
      type: typesArray,
      moves: movesArray,
      image: images[0],
    });
    await newPokemon.save();
    console.log(`################################################`);
    console.log(`Got data for ${newPokemon.name}`);
    console.log(newPokemon);
    console.log(`################################################`);
  });
  console.log("finished");
  // CODE FOR GETTING ALL THE MOVES

  const {
    data: { results: allMoves },
  } = await Axios.get("https://pokeapi.co/api/v2/move/?limit=1000");
  await Move.deleteMany({});
  allMoves.map(async (move: { url: string; name: string }) => {
    const { data: moveDetails } = await Axios.get(move.url);
    const newMove = new Move({
      name: moveDetails.name,
      pp: moveDetails.pp || 0,
      power: moveDetails.power || 0,
      accuracy: moveDetails.accuracy || 0,
      type: moveDetails.type.name || 0,
      effect_chance: moveDetails.effect_chance || 0,
      generation: moveDetails.generation.name,
    });
    await newMove.save();
  });
};

ToplevelAwait();
