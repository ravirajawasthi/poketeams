import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import Pokemon from "./models/Pokemon";
require("./models/Move");

import { MONGO_PASSWORD } from "./.env";

mongoose.connect(
  `mongodb+srv://bablu:${MONGO_PASSWORD}@pokemonteam.wcyto.mongodb.net/PokeTeams?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (_req, res) => {
  res.send("Hello");
});

app.get("/pokemons", async (_req, res) => {
  console.log("here");
  const AllPokemons = await Pokemon.find({}).populate("moves");
  console.log("here2");
  res.send(AllPokemons);
});

app.listen(3000, () => {
  console.log("port 3000");
});
