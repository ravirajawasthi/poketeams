import mongoose from "mongoose";

interface GameDoc extends mongoose.Document {
  id: string;
  nationalDexStarting: number;
  nationalDexEnding: number;
  exceptions: [number];
  region: string;
  launchDate: Date;
  launchConsole: {
    name: string;
    image: string;
  };
}

const gameSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    nationalDexStarting: { type: Number, required: true },
    nationalDexEnding: { type: Number, required: true },
    exceptions: { type: Array, default: [] },
    region: { type: String, required: true },
    launchDate: {
      type: Date,
      required: true,
    },
    launchConsole: {
      name: {
        type: String,
        required: true,
      },
      image: {
        type: String,
        required: true,
      },
    },
  },
  {
    toJSON: {
      transform(_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
      },
    },
  }
);
const Game = mongoose.model<GameDoc>("Game", gameSchema);

export default Game;
