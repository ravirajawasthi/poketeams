enum PokemonTypes {
  normal = "Normal",
  fighting = "Fighting",
  fire = "Fire",
  water = "Water",
  grass = "Grass",
  electric = "Electric",
  ice = "Ice",
  poison = "Poison",
  ground = "Ground",
  flying = "Flying",
  psychic = "Psychic",
  bug = "Bug",
  rock = "Rock",
  ghost = "Ghost",
  dark = "Dark",
  dragon = "Dragon",
  steel = "Steel",
  fairy = "Fairy",
}

interface Move {
  name: string;
  pp: number;
  type: PokemonTypes;
  power: number;
  accuracy: number;
  generation: string;
  effect_change: number;
}

interface Pokemon {
  id: string;
  name: string;
  type: [PokemonTypes];
  starter: boolean;
  legandary: boolean;
  nationDex: number;
  regionalDex: number;
  image: string;
}

export { PokemonTypes, Move, Pokemon };
