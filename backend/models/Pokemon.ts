import mongoose from "mongoose";
import { PokemonTypes } from "./interface";

interface PokemonDoc extends mongoose.Document {
  id: string;
  name: string;
  type: [PokemonTypes];
  nationDex: number;
  image: string;
}

const pokemonSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    nationalDex: { type: Number, required: true },

    type: {
      type: Array,
      required: true,
    },

    image: {
      type: String,
      required: true,
    },
    moves: [{ type: mongoose.Types.ObjectId, ref: "Move" }],
  },
  {
    toJSON: {
      transform(_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
      },
    },
  }
);
const Pokemon = mongoose.model<PokemonDoc>("Pokemon", pokemonSchema);

export default Pokemon;
