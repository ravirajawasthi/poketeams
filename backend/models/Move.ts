import mongoose from "mongoose";
import { PokemonTypes } from "./interface";

interface MoveDoc extends mongoose.Document {
  name: string;
  pp: number;
  type: PokemonTypes;
  power: number;
  accuracy: number;
  generation: string;
  effect_chance: number;
}

const moveSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    pp: { type: Number, required: true },
    type: { type: String, required: true },
    generation: { type: String, required: true },
    effect_chance: { type: Number, required: true },
    power: {
      type: Number,
      required: true,
    },
    accuracy: {
      type: Number,
      required: true,
    },
  },
  {
    toJSON: {
      transform(_doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
      },
    },
  }
);
const Move = mongoose.model<MoveDoc>("Move", moveSchema);

export default Move;
